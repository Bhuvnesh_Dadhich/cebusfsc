package in.tdt.utils;

public class APICredential {

    private String clientId;

    private String clientSecret;

    private String username;

    private String password;

    public APICredential(String clientId, String clientSecret,
                         String username, String password) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.username = username;
        this.password = password;
    }

    protected String getClientId() {
        return this.clientId;
    }

    protected String getClientSecret() {
        return this.clientSecret;
    }

    protected String getUsername() {
        return this.username;
    }

    protected String getPassword() {
        return this.password;
    }

}
