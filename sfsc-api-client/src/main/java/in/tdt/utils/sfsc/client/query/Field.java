package in.tdt.utils.sfsc.client.query;

public class Field {

    private Integer byteLength;

    private String name;

    public Integer getByteLength() {
        return byteLength;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Field{" +
                "byteLength=" + byteLength +
                ", name='" + name + '\'' +
                '}';
    }
}
