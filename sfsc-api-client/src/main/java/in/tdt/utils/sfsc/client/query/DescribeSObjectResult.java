package in.tdt.utils.sfsc.client.query;

import java.util.List;

public class DescribeSObjectResult {

    private boolean activateable;

    private boolean createable;

    private String name;

    private List<Field> fields;

    public boolean isActivateable() {
        return activateable;
    }

    public boolean isCreateable() {
        return createable;
    }

    public String getName() {
        return name;
    }

    public List<Field> getFields() {
        return fields;
    }

    @Override
    public String toString() {
        return "DescribeSObjectResult{" +
                "activateable=" + activateable +
                ", createable=" + createable +
                ", name='" + name + '\'' +
                ", fields=" + fields +
                '}';
    }
}
