package in.tdt.utils.sfsc.client.query;

import com.github.scribejava.core.model.*;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;
import com.google.gson.internal.GsonBuildConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class ExecuteQueryRequest {

    private static Logger log = LoggerFactory.getLogger(ExecuteQueryRequest.class.getName());

    private static final Gson gson = new GsonBuilder()
            .serializeNulls()
            .serializeSpecialFloatingPointValues()
            .setLongSerializationPolicy(LongSerializationPolicy.STRING)
            .create();

    public static JobSubmitResponse runQueryRequest(OAuthUtil util, String baseURI,
                                                    QueryRequest queryRequest) throws IOException, ExecutionException, InterruptedException {
        OAuthRequest request = new OAuthRequest(Verb.POST, baseURI + "/jobs/query");
        request.setPayload(gson.toJson(queryRequest));
        request.addHeader("Content-Type", "application/json");

        Response response = util.executeRequest(request);
        log.debug("Response Status: " + response.getCode());
        log.debug("Response Message: " + response.getMessage());

        if(response.isSuccessful()) {
            String body = response.getBody();
            log.debug("Response Body: " + body);
            response.close();
            return gson.fromJson(body, JobSubmitResponse.class);
        } else {
            return null;
        }

    }

    public static JobStatus getJobStatus(OAuthUtil util, String baseURI,
                                         String jobId) throws IOException, ExecutionException, InterruptedException {
        JobStatus status = null;
        OAuthRequest request = new OAuthRequest(Verb.GET, baseURI + "/jobs/query/" + jobId);
        Response response = util.executeRequest(request);
        log.debug("Response Status: " + response.getCode());
        log.debug("Response Message: " + response.getMessage());
        if(response.isSuccessful()) {
            status = gson.fromJson(response.getBody(), JobStatus.class);
            log.debug("Job Status: " + status);
        } else {
            status = new JobStatus();
            status.setId(jobId);
            status.setState("UNKNOWN");
        }
        response.close();
        return status;
    }

    public static DescribeSObjectResult getObjectMetadata(OAuthUtil util, String baseURI, String objectName)
            throws IOException, ExecutionException, InterruptedException {
        DescribeSObjectResult result = null;
        OAuthRequest request = new OAuthRequest(Verb.GET, baseURI + "/sobjects/" + objectName + "/describe");
        Response response = util.executeRequest(request);
        log.debug("Response Status: " + response.getCode());
        log.debug("Response Message: " + response.getMessage());
        if(response.isSuccessful()) {
            result = gson.fromJson(response.getBody(), DescribeSObjectResult.class);
            log.debug("SObject Metadata: " + result);
        }
        response.close();
        return result;
    }

    public static boolean isJobComplete(OAuthUtil util, String baseURI,
                                        String jobId) throws IOException, ExecutionException, InterruptedException {
        boolean jobComplete = false;
        JobStatus status = null;
        OAuthRequest request = new OAuthRequest(Verb.GET, baseURI + "/jobs/query/" + jobId);
        Response response = util.executeRequest(request);
        log.debug("Response Status: " + response.getCode());
        log.debug("Response Message: " + response.getMessage());
        if(response.isSuccessful()) {
            status = gson.fromJson(response.getBody(), JobStatus.class);
            log.debug("Job Status: " + status);
            jobComplete = status != null && status.getState() != null && status.getState().toLowerCase().contains("jobcomplete");
        } else {
            jobComplete = false;
        }
        response.close();
        return jobComplete;
    }

    public static boolean isJobComplete(OAuthUtil util, String baseURI,
                                          String jobId, long waitTime) throws IOException, ExecutionException, InterruptedException {

        boolean jobComplete = false;
        int maxTries = (int) (waitTime/2000L);
        int tries = 1;
        JobStatus status = null;
        Response response = null;
        do {
            log.debug("Try: " + tries);
            OAuthRequest request = new OAuthRequest(Verb.GET, baseURI + "/jobs/query/" + jobId);
            response = util.executeRequest(request);
            log.debug("Response Status: " + response.getCode());
            log.debug("Response Message: " + response.getMessage());
            if(response.isSuccessful()) {
                status = gson.fromJson(response.getBody(), JobStatus.class);
                log.debug("Job Status: " + status);
                jobComplete = status != null && status.getState() != null && status.getState().toLowerCase().contains("jobcomplete");
            } else {
                jobComplete = false;
            }
            tries++;
            try {
                Thread.sleep(2000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while(!jobComplete && tries < maxTries);
        if(response != null)
            response.close();
        return jobComplete;
    }

}
