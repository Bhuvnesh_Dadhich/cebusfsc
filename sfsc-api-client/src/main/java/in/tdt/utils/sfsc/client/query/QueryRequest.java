package in.tdt.utils.sfsc.client.query;

/**
 * Wrapper for the request body
 */
public class QueryRequest {

    private String operation;
    private String query;
    private String contentType;
    private String columnDelimiter;
    private String lineEnding;

    public QueryRequest(String operation, String query, String contentType,
                        String columnDelimiter, String lineEnding) {
        this.operation = operation;
        this.query = query;
        this.contentType = contentType;
        this.columnDelimiter = columnDelimiter;
        this.lineEnding = lineEnding;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getColumnDelimiter() {
        return columnDelimiter;
    }

    public void setColumnDelimiter(String columnDelimiter) {
        this.columnDelimiter = columnDelimiter;
    }

    public String getLineEnding() {
        return lineEnding;
    }

    public void setLineEnding(String lineEnding) {
        this.lineEnding = lineEnding;
    }
}
