package in.tdt.utils.sfsc.client.query;

import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import org.apache.commons.io.IOUtils;


import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

public class ExecuteJobResult implements Iterator<JobResultOutput> {

    private String url;

    private String jobId;

    private String maxRecords;

    private String locator = "init";

    private OAuthUtil util;

    private Response response;

    public ExecuteJobResult(String url, String jobId, String maxRecords, OAuthUtil util) {
        this.url = url;
        this.jobId = jobId;
        this.maxRecords = maxRecords;
        this.util = util;
    }

    public ExecuteJobResult(String url, String jobId, String maxRecords, OAuthUtil util, String locator) {
        this.url = url;
        this.jobId = jobId;
        this.maxRecords = maxRecords;
        this.util = util;
        this.locator = locator;
    }

    public String getLocator() {
        return locator;
    }

    @Override
    public boolean hasNext() {
        return ! "null".equalsIgnoreCase(locator);
    }

    @Override
    public JobResultOutput next() {
        JobResultOutput out = null;
        OAuthRequest request = new OAuthRequest(Verb.GET, url + "/jobs/query/" + jobId + "/results");
        request.addParameter("maxRecords", maxRecords);
        if("init".equalsIgnoreCase(locator)) {
            //First Request
            //Do not add locator param
        } else {
            request.addParameter("locator", locator);
        }
        try {
            response = util.executeRequest(request);
            if(response.isSuccessful()) {
                locator = response.getHeader("Sforce-Locator");
                String numberOfRecords = response.getHeader("Sforce-NumberOfRecords");
                long recordCount = numberOfRecords != null && !"".equalsIgnoreCase(numberOfRecords)?Long.parseLong(numberOfRecords.trim()):0L;
                out = new JobResultOutput(recordCount, response);
                //response.close();
                return out;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
