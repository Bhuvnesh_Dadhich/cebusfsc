package in.tdt.utils.sfsc.client.query;

public class JobStatus {

    private String id;

    private String operation;

    private String object;

    private String createdBy;

    private String createdDate;

    private String systemModstamp;

    private String state;

    private String concurrencyMode;

    private String contentType;

    private String apiVersion;

    private String jobType;

    private String lineEnding;

    private String columnDelimiter;

    private Long numberRecordsProcessed;

    private Long retries;

    private Long totalProcessingTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getSystemModstamp() {
        return systemModstamp;
    }

    public void setSystemModstamp(String systemModstamp) {
        this.systemModstamp = systemModstamp;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getConcurrencyMode() {
        return concurrencyMode;
    }

    public void setConcurrencyMode(String concurrencyMode) {
        this.concurrencyMode = concurrencyMode;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getLineEnding() {
        return lineEnding;
    }

    public void setLineEnding(String lineEnding) {
        this.lineEnding = lineEnding;
    }

    public String getColumnDelimiter() {
        return columnDelimiter;
    }

    public void setColumnDelimiter(String columnDelimiter) {
        this.columnDelimiter = columnDelimiter;
    }

    public Long getNumberRecordsProcessed() {
        return numberRecordsProcessed;
    }

    public void setNumberRecordsProcessed(Long numberRecordsProcessed) {
        this.numberRecordsProcessed = numberRecordsProcessed;
    }

    public Long getRetries() {
        return retries;
    }

    public void setRetries(Long retries) {
        this.retries = retries;
    }

    public Long getTotalProcessingTime() {
        return totalProcessingTime;
    }

    public void setTotalProcessingTime(Long totalProcessingTime) {
        this.totalProcessingTime = totalProcessingTime;
    }

    @Override
    public String toString() {
        return "JobStatus{" +
                "id='" + id + '\'' +
                ", operation='" + operation + '\'' +
                ", object='" + object + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", systemModstamp='" + systemModstamp + '\'' +
                ", state='" + state + '\'' +
                ", concurrencyMode='" + concurrencyMode + '\'' +
                ", contentType='" + contentType + '\'' +
                ", apiVersion='" + apiVersion + '\'' +
                ", jobType='" + jobType + '\'' +
                ", lineEnding='" + lineEnding + '\'' +
                ", columnDelimiter='" + columnDelimiter + '\'' +
                ", numberRecordsProcessed=" + numberRecordsProcessed +
                ", retries=" + retries +
                ", totalProcessingTime=" + totalProcessingTime +
                '}';
    }
}
