package in.tdt.utils.sfsc.client.query;

import com.github.scribejava.core.httpclient.HttpClientConfig;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.oauth2.clientauthentication.RequestBodyAuthenticationScheme;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.oauth.OAuth20Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class OAuthUtil {

    private SFSCClient client;

    private OAuth20Service service = null;

    private String username;

    private String password;

    private static Logger log = LoggerFactory.getLogger(ExecuteQueryRequest.class.getName());

    public OAuthUtil(SFSCClient client, String username, String password) {
        this.client = client;
        this.username = username;
        this.password = password;
        this.service = new ServiceBuilder(client.getClientId())
                .apiSecret(client.getClientSecret())
                .build(SFSCQueryAPI.instance());
    }

    public OAuth2AccessToken getAccessToken() throws InterruptedException,
            ExecutionException, IOException {

        OAuth2AccessToken token = null;
        try {
            token = service.getAccessTokenPasswordGrant(username, password);
        } catch (Exception e) {
            log.error("Exception in getting Token: " + e.getMessage());
        }
        return token;
    }

    private void signRequest(OAuthRequest request) throws InterruptedException, ExecutionException, IOException {
        service.signRequest(getAccessToken(), request);
    }

    private void signRequest(OAuthRequest request, String token) throws InterruptedException, ExecutionException, IOException {
        service.signRequest(token, request);
    }

    public Response executeRequest(OAuthRequest request) throws InterruptedException, ExecutionException, IOException {
        signRequest(request);
        return service.execute(request);
    }

    public Response executeRequest(OAuthRequest request, String token) throws InterruptedException, ExecutionException, IOException {
        signRequest(request, token);
        return service.execute(request);
    }

}
