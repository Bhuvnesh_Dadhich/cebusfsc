package in.tdt.utils.sfsc.client.query;

import com.github.scribejava.core.model.Response;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class JobResultOutput implements Closeable {

    private long numberOfRecords;

    private Response response;

    public JobResultOutput(long numberOfRecords, Response response) {
        this.numberOfRecords = numberOfRecords;
        this.response = response;
    }

    public long getNumberOfRecords() {
        return numberOfRecords;
    }

    public Response getResponse() {
        return response;
    }

    @Override
    public void close() throws IOException {
        if(response != null) {
            response.close();
        }
    }
}
