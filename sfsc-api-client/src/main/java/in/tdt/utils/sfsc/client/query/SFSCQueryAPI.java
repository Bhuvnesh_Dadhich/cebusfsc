package in.tdt.utils.sfsc.client.query;

import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.oauth2.clientauthentication.ClientAuthentication;
import com.github.scribejava.core.oauth2.clientauthentication.RequestBodyAuthenticationScheme;

public class SFSCQueryAPI extends DefaultApi20 {

    public SFSCQueryAPI() {}

    private static class InstanceHolder {
        private static final SFSCQueryAPI INSTANCE = new SFSCQueryAPI();
    }

    public static SFSCQueryAPI instance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public String getAccessTokenEndpoint() {
        return "https://login.salesforce.com/services/oauth2/token";
    }

    @Override
    protected String getAuthorizationBaseUrl() {
        return null;
    }

    @Override
    public ClientAuthentication getClientAuthentication() {
        return RequestBodyAuthenticationScheme.instance();
    }
}
