/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package in.tdt.utils.processors.SFServiceCloud;

import in.tdt.utils.sfsc.client.query.*;
import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.util.StandardValidators;

import java.util.*;

@Tags({"Salesforce", "Salesforce Service Cloud", "SFSC"})
@CapabilityDescription("This processor gives makes a query request to Salesforce Bulk API 2.0 provided required parameters and attributes. " +
        "This processor writes the response content to flowfiles.")
@SeeAlso({SFSCCheckJobStatusProcessor.class, SFSCExtractQueryDataProcessor.class})
@ReadsAttributes({@ReadsAttribute(attribute="query", description="They SFSC Query to be executed"),
        @ReadsAttribute(attribute="contentType",description="Expected Response content type from SFSC Query API"),
        @ReadsAttribute(attribute="columnDelimiter",description="Delimiter for CSV content type"),
        @ReadsAttribute(attribute="lineEnding",description="Line Ending for CSV content type")}
)
@WritesAttributes({@WritesAttribute(attribute="sfsc.job.id", description="The id of the submitted job")})
public class SFSCQueryProcessor extends AbstractProcessor {

    public static final PropertyDescriptor CLIENT_ID = new PropertyDescriptor
            .Builder().name("CLIENT_ID")
            .displayName("Client ID")
            .description("SFSC Client ID")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor CLIENT_SECRET = new PropertyDescriptor
            .Builder().name("CLIENT_SECRET")
            .displayName("Client Secret")
            .description("SFSC Client Secret")
            .required(true)
            .sensitive(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor USERNAME = new PropertyDescriptor
            .Builder().name("USERNAME")
            .displayName("Username")
            .description("Username")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor PASSWORD = new PropertyDescriptor
            .Builder().name("PASSWORD")
            .displayName("Password")
            .description("Password")
            .required(true)
            .sensitive(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor BASE_URL = new PropertyDescriptor
            .Builder().name("BASE_URL")
            .displayName("Base URL")
            .description("Base URL from while API endpoints can be accessed")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor TOKEN_REQUEST_URL = new PropertyDescriptor
            .Builder().name("TOKEN_REQUEST_URL")
            .displayName("Token Request URL")
            .description("URL at which access tokens can be requested.")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final Relationship SUCCESS = new Relationship.Builder()
            .name("success")
            .description("Success")
            .build();

    public static final Relationship FAILURE = new Relationship.Builder()
            .name("failure")
            .description("Failure")
            .build();

    private List<PropertyDescriptor> descriptors;

    private Set<Relationship> relationships;

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();
        descriptors.add(CLIENT_ID);
        descriptors.add(CLIENT_SECRET);
        descriptors.add(USERNAME);
        descriptors.add(PASSWORD);
        descriptors.add(BASE_URL);
        descriptors.add(TOKEN_REQUEST_URL);

        this.descriptors = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<Relationship>();
        relationships.add(SUCCESS);
        relationships.add(FAILURE);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }

    @OnScheduled
    public void onScheduled(final ProcessContext context) {

    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if ( flowFile == null ) {
            return;
        }
        String clientId = context.getProperty("CLIENT_ID")
                .evaluateAttributeExpressions().getValue();
        String clientSecret = context.getProperty("CLIENT_SECRET")
                .evaluateAttributeExpressions().getValue();
        String username = context.getProperty("USERNAME")
                .evaluateAttributeExpressions().getValue();
        String password = context.getProperty("PASSWORD")
                .evaluateAttributeExpressions().getValue();
        String baseURL = context.getProperty("BASE_URL")
                .evaluateAttributeExpressions().getValue();
        String tokenRequestURL = context.getProperty("TOKEN_REQUEST_URL")
                .evaluateAttributeExpressions().getValue();

        String operation = "query";
        String query = flowFile.getAttribute("query");
        String contentType = flowFile.getAttribute("contentType");
        String columnDelimiter = flowFile.getAttribute("columnDelimiter");
        String lineEnding = flowFile.getAttribute("lineEnding");

        SFSCClient client = new SFSCClient(clientId, clientSecret);
        OAuthUtil oAuthUtil = new OAuthUtil(client, username, password);
        QueryRequest request = new QueryRequest(operation, query, contentType, columnDelimiter, lineEnding);

        try {
            JobSubmitResponse jobSubmitResponse =
                    ExecuteQueryRequest.runQueryRequest(oAuthUtil,baseURL, request );
            session.putAttribute(flowFile, "sfsc.job.id", jobSubmitResponse.getId());
            session.putAttribute(flowFile, "sfsc.job.status", jobSubmitResponse.getState());
            session.putAttribute(flowFile, "sfsc.job.operation", jobSubmitResponse.getOperation());
            session.putAttribute(flowFile, "sfsc.job.columnDelimiter", jobSubmitResponse.getColumnDelimiter());
            session.putAttribute(flowFile, "sfsc.job.lineEnding", jobSubmitResponse.getLineEnding());
            session.putAttribute(flowFile, "sfsc.job.createdDate", jobSubmitResponse.getCreatedDate());
            session.putAttribute(flowFile, "sfsc.job.createdBy", jobSubmitResponse.getCreatedById());
            session.getProvenanceReporter().modifyAttributes(flowFile);
            session.transfer(flowFile, SUCCESS);
//            boolean isJobComplete = ExecuteQueryRequest.isJobComplete(oAuthUtil,
//                    baseURL, jobSubmitResponse.getId(), waitTime);
//            AtomicInteger index = new AtomicInteger(0);
//            int i = 0;
//            if(isJobComplete) {
//                ExecuteJobResult executeJobResult = new ExecuteJobResult(baseURL, jobSubmitResponse.getId(), maxRecords,
//                        oAuthUtil);
//
//                while(executeJobResult.hasNext()) {
//                    JobResultOutput output = executeJobResult.next();
//                    out = session.create(flowFile);
//                    out = session.write(out, outputStream -> outputStream.write(output.getBuffer().array()));
//                    session.putAttribute(out, "fragment.index", String.valueOf(index));
//                    session.putAttribute(out, "record.count", String.valueOf(output.getNumberOfRecords()));
//                    index.set(i++);
//                    session.transfer(out, RESPONSE);
//                }
//                session.transfer(flowFile, ORIGINAL);
//            } else {
//                session.transfer(flowFile, TIMED_OUT);
//            }
            //session.transfer(flowFiles, RESPONSE);
        } catch (Exception e) {
            e.printStackTrace();
            getLogger().error("Exception while fetching data from SFSC: " + e.getMessage());
            session.transfer(flowFile, FAILURE);
        }
    }
}
