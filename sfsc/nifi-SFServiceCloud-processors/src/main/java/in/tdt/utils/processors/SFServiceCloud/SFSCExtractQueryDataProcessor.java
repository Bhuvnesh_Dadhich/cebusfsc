package in.tdt.utils.processors.SFServiceCloud;

import in.tdt.utils.sfsc.client.query.ExecuteJobResult;
import in.tdt.utils.sfsc.client.query.JobResultOutput;
import in.tdt.utils.sfsc.client.query.OAuthUtil;
import in.tdt.utils.sfsc.client.query.SFSCClient;
import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.util.StandardValidators;

import java.io.InputStream;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Tags({"Salesforce", "Salesforce Service Cloud", "SFSC"})
@CapabilityDescription("Given a Job ID, this processor checks whether a job is complete and if completed, extracts job data. " +
        "If not routes the flowfile to IN PROGRESS queue." +
        "This processor writes the response content to flowfiles.")
@SeeAlso({SFSCQueryProcessor.class, SFSCCheckJobStatusProcessor.class})
@ReadsAttributes({@ReadsAttribute(attribute="sfsc.job.id", description="The SFSC Query to be executed"),
        @ReadsAttribute(attribute="sfsc.job.status", description="The status of job from SFSCCheckJobStatusProcessor"),
        @ReadsAttribute(attribute="sfsc.job.locator", description="The point from which data has to be exported " +
                "(based on Sforce-Locator header from response)")}
)
@WritesAttributes({@WritesAttribute(attribute="fragment.index", description="The index of the response file"),
        @WritesAttribute(attribute="record.count", description="The number of records in this specific response fragment")})
public class SFSCExtractQueryDataProcessor  extends AbstractProcessor {

    public static final PropertyDescriptor CLIENT_ID = new PropertyDescriptor
            .Builder().name("CLIENT_ID")
            .displayName("Client ID")
            .description("SFSC Client ID")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor CLIENT_SECRET = new PropertyDescriptor
            .Builder().name("CLIENT_SECRET")
            .displayName("Client Secret")
            .description("SFSC Client Secret")
            .required(true)
            .sensitive(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor USERNAME = new PropertyDescriptor
            .Builder().name("USERNAME")
            .displayName("Username")
            .description("Username")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor PASSWORD = new PropertyDescriptor
            .Builder().name("PASSWORD")
            .displayName("Password")
            .description("Password")
            .required(true)
            .sensitive(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor BASE_URL = new PropertyDescriptor
            .Builder().name("BASE_URL")
            .displayName("Base URL")
            .description("Base URL from while API endpoints can be accessed")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor TOKEN_REQUEST_URL = new PropertyDescriptor
            .Builder().name("TOKEN_REQUEST_URL")
            .displayName("Token Request URL")
            .description("URL at which access tokens can be requested.")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor MAX_FETCH_PER_REQUEST = new PropertyDescriptor
            .Builder().name("MAX_FETCH_PER_REQUEST")
            .displayName("Max Records")
            .description("Maximum number of records to be fetched in each request.")
            .required(true)
            .defaultValue("1000")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .addValidator(StandardValidators.NUMBER_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final Relationship RESPONSE = new Relationship.Builder()
            .name("response")
            .description("All files from API response are written to this queue.")
            .build();

    public static final Relationship FAILURE = new Relationship.Builder()
            .name("failure")
            .description("The original flowfile in case of failures are written to this queue.")
            .build();

    public static final Relationship IN_COMPLETE = new Relationship.Builder()
            .name("incomplete")
            .description("If the job id given in attribute sfsc.job.id is not complete, original flowfile is written to this queue.")
            .build();

    public static final Relationship ORIGINAL = new Relationship.Builder()
            .name("original")
            .description("The original flowfile is routed to this relationship upon successful execution of Job Data fetch cycle.")
            .build();

    private List<PropertyDescriptor> descriptors;

    private Set<Relationship> relationships;

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();
        descriptors.add(CLIENT_ID);
        descriptors.add(CLIENT_SECRET);
        descriptors.add(USERNAME);
        descriptors.add(PASSWORD);
        descriptors.add(BASE_URL);
        descriptors.add(TOKEN_REQUEST_URL);
        descriptors.add(MAX_FETCH_PER_REQUEST);

        this.descriptors = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<Relationship>();
        relationships.add(RESPONSE);
        relationships.add(FAILURE);
        relationships.add(IN_COMPLETE);
        relationships.add(ORIGINAL);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }

    @OnScheduled
    public void onScheduled(final ProcessContext context) {

    }

    @Override
    public void onTrigger(ProcessContext context, ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        FlowFile out = null;
        if ( flowFile == null ) {
            return;
        }
        String clientId = context.getProperty("CLIENT_ID")
                .evaluateAttributeExpressions().getValue();
        String clientSecret = context.getProperty("CLIENT_SECRET")
                .evaluateAttributeExpressions().getValue();
        String username = context.getProperty("USERNAME")
                .evaluateAttributeExpressions().getValue();
        String password = context.getProperty("PASSWORD")
                .evaluateAttributeExpressions().getValue();
        String baseURL = context.getProperty("BASE_URL")
                .evaluateAttributeExpressions().getValue();
        String tokenRequestURL = context.getProperty("TOKEN_REQUEST_URL")
                .evaluateAttributeExpressions().getValue();
        String maxRecords =  context.getProperty("MAX_FETCH_PER_REQUEST")
                .evaluateAttributeExpressions().getValue();

        String jobId = flowFile.getAttribute("sfsc.job.id");
        String jobStatus = flowFile.getAttribute("sfsc.job.status");
        String locator = flowFile.getAttribute("sfsc.job.locator");
        if(locator == null || "".equals(locator.trim())) {
            locator = "init";
        }

        String totalRecords = flowFile.getAttribute("record.count");
        long totalRec = 0;
        if(totalRecords != null && !"".equals(totalRecords.trim())) {
            totalRec = Long.parseLong(totalRecords);
        }


        long totalFiles = 0;
        String fragments = flowFile.getAttribute("total.file.count");
        if(fragments != null && !"".equals(fragments.trim())) {
            totalFiles = Long.parseLong(fragments.trim());
        } else {
            totalFiles = 0;
        }

        SFSCClient client = new SFSCClient(clientId, clientSecret);
        OAuthUtil oAuthUtil = new OAuthUtil(client, username, password);
        try {
            boolean isJobComplete = (jobStatus != null && jobStatus.toLowerCase().contains("jobcomplete"));
            //Assume job is complete if locator is not null
            if(isJobComplete) {
                byte[] buf = new byte[8192];

                ExecuteJobResult executeJobResult = new ExecuteJobResult(baseURL, jobId, maxRecords,
                        oAuthUtil, locator);
                if(executeJobResult.hasNext()) {
                    JobResultOutput jobResultOutput = executeJobResult.next();
                    out = session.create(flowFile);
                    out = session.write(out, outputStream -> {
                        int c = -1;
                        InputStream inputStream = jobResultOutput.getResponse().getStream();
                        while ((c = inputStream.read(buf)) > 0) {
                            outputStream.write(buf, 0, c);
                        }
                        outputStream.flush();
                        outputStream.close();
                        jobResultOutput.close();
                    });

                    session.putAttribute(out, "record.count", String.valueOf(jobResultOutput.getNumberOfRecords()));

                    session.putAttribute(flowFile,"record.count", String.valueOf(totalRec + jobResultOutput.getNumberOfRecords()));
                    session.putAttribute(flowFile,"total.file.count", String.valueOf(totalFiles +1));
                    session.putAttribute(flowFile, "sfsc.job.locator", executeJobResult.getLocator());

                    session.transfer(out, RESPONSE);
                    session.transfer(flowFile, ORIGINAL);
                } else {
                    session.putAttribute(flowFile, "sfsc.job.locator", "");
                    session.transfer(flowFile, ORIGINAL);
                }
            } else {
                session.transfer(flowFile, IN_COMPLETE);
            }
            //session.transfer(flowFiles, RESPONSE);
        } catch (Exception e) {
            e.printStackTrace();
            getLogger().error("Exception while fetching data from SFSC: " + e.getMessage());
            session.transfer(flowFile, FAILURE);
        }
    }
}
