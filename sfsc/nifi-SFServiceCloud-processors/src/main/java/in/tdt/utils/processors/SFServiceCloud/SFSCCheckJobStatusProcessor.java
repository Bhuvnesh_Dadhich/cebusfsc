package in.tdt.utils.processors.SFServiceCloud;

import in.tdt.utils.sfsc.client.query.ExecuteQueryRequest;
import in.tdt.utils.sfsc.client.query.JobStatus;
import in.tdt.utils.sfsc.client.query.OAuthUtil;
import in.tdt.utils.sfsc.client.query.SFSCClient;
import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.util.StandardValidators;

import java.util.*;

@Tags({"Salesforce", "Salesforce Service Cloud", "SFSC"})
@CapabilityDescription("Given a Job ID, this processor checks for the job status. ")
@SeeAlso({SFSCQueryProcessor.class, SFSCExtractQueryDataProcessor.class})
@ReadsAttributes({@ReadsAttribute(attribute="sfsc.job.id", description="They SFSC Query to be executed")}
)
@WritesAttributes({@WritesAttribute(attribute="sfsc.job.status", description="Job Status")})
public class SFSCCheckJobStatusProcessor extends AbstractProcessor {

    public static final PropertyDescriptor CLIENT_ID = new PropertyDescriptor
            .Builder().name("CLIENT_ID")
            .displayName("Client ID")
            .description("SFSC Client ID")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor CLIENT_SECRET = new PropertyDescriptor
            .Builder().name("CLIENT_SECRET")
            .displayName("Client Secret")
            .description("SFSC Client Secret")
            .required(true)
            .sensitive(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor USERNAME = new PropertyDescriptor
            .Builder().name("USERNAME")
            .displayName("Username")
            .description("Username")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor PASSWORD = new PropertyDescriptor
            .Builder().name("PASSWORD")
            .displayName("Password")
            .description("Password")
            .required(true)
            .sensitive(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor BASE_URL = new PropertyDescriptor
            .Builder().name("BASE_URL")
            .displayName("Base URL")
            .description("Base URL from while API endpoints can be accessed")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor TOKEN_REQUEST_URL = new PropertyDescriptor
            .Builder().name("TOKEN_REQUEST_URL")
            .displayName("Token Request URL")
            .description("URL at which access tokens can be requested.")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final Relationship SUCCESS = new Relationship.Builder()
            .name("success")
            .description("Job status fetch Success")
            .build();

    public static final Relationship FAILURE = new Relationship.Builder()
            .name("failure")
            .description("The original flowfile in case of failures are written to this queue.")
            .build();

    private List<PropertyDescriptor> descriptors;

    private Set<Relationship> relationships;

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();
        descriptors.add(CLIENT_ID);
        descriptors.add(CLIENT_SECRET);
        descriptors.add(USERNAME);
        descriptors.add(PASSWORD);
        descriptors.add(BASE_URL);
        descriptors.add(TOKEN_REQUEST_URL);

        this.descriptors = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<Relationship>();
        relationships.add(SUCCESS);
        relationships.add(FAILURE);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }

    @OnScheduled
    public void onScheduled(final ProcessContext context) {

    }

    @Override
    public void onTrigger(ProcessContext context, ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if ( flowFile == null ) {
            return;
        }
        String clientId = context.getProperty("CLIENT_ID")
                .evaluateAttributeExpressions().getValue();
        String clientSecret = context.getProperty("CLIENT_SECRET")
                .evaluateAttributeExpressions().getValue();
        String username = context.getProperty("USERNAME")
                .evaluateAttributeExpressions().getValue();
        String password = context.getProperty("PASSWORD")
                .evaluateAttributeExpressions().getValue();
        String baseURL = context.getProperty("BASE_URL")
                .evaluateAttributeExpressions().getValue();
        String tokenRequestURL = context.getProperty("TOKEN_REQUEST_URL")
                .evaluateAttributeExpressions().getValue();

        String jobId = flowFile.getAttribute("sfsc.job.id");

        SFSCClient client = new SFSCClient(clientId, clientSecret);
        OAuthUtil oAuthUtil = new OAuthUtil(client, username, password);

        try {
            JobStatus status = ExecuteQueryRequest.getJobStatus(oAuthUtil, baseURL, jobId);
            session.putAttribute(flowFile, "sfsc.job.status", status.getState());
            session.getProvenanceReporter().modifyAttributes(flowFile);
            session.transfer(flowFile, SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            getLogger().error("Exception while job status from SFSC for jobID " + jobId + ": " + e.getMessage());
            session.transfer(flowFile, FAILURE);
        }
    }
}
