package in.tdt.utils.processors.SFServiceCloud;


import com.google.gson.Gson;
import in.tdt.utils.sfsc.client.query.DescribeSObjectResult;
import in.tdt.utils.sfsc.client.query.ExecuteQueryRequest;
import in.tdt.utils.sfsc.client.query.OAuthUtil;
import in.tdt.utils.sfsc.client.query.SFSCClient;
import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.io.OutputStreamCallback;
import org.apache.nifi.processor.util.StandardValidators;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

@Tags({"Salesforce", "Salesforce Service Cloud", "SFSC"})
@CapabilityDescription("This processor returns the list of fields for a given object as JSON. ")
@SeeAlso({SFSCCheckJobStatusProcessor.class, SFSCExtractQueryDataProcessor.class, SFSCQueryProcessor.class})
@ReadsAttributes({@ReadsAttribute(attribute="sfsc.object.name", description="They SFSC Query to be executed")})
@WritesAttributes({@WritesAttribute(attribute="field.count", description="The number of fields in the object")})
public class SFSCGetObjectMetadataProcessor  extends AbstractProcessor {

    public static final PropertyDescriptor CLIENT_ID = new PropertyDescriptor
            .Builder().name("CLIENT_ID")
            .displayName("Client ID")
            .description("SFSC Client ID")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor CLIENT_SECRET = new PropertyDescriptor
            .Builder().name("CLIENT_SECRET")
            .displayName("Client Secret")
            .description("SFSC Client Secret")
            .required(true)
            .sensitive(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor USERNAME = new PropertyDescriptor
            .Builder().name("USERNAME")
            .displayName("Username")
            .description("Username")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor PASSWORD = new PropertyDescriptor
            .Builder().name("PASSWORD")
            .displayName("Password")
            .description("Password")
            .required(true)
            .sensitive(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor BASE_URL = new PropertyDescriptor
            .Builder().name("BASE_URL")
            .displayName("Base URL")
            .description("Base URL from while API endpoints can be accessed")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor TOKEN_REQUEST_URL = new PropertyDescriptor
            .Builder().name("TOKEN_REQUEST_URL")
            .displayName("Token Request URL")
            .description("URL at which access tokens can be requested.")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final Relationship SUCCESS = new Relationship.Builder()
            .name("success")
            .description("Success")
            .build();

    public static final Relationship FAILURE = new Relationship.Builder()
            .name("failure")
            .description("Failure")
            .build();

    private List<PropertyDescriptor> descriptors;

    private Set<Relationship> relationships;

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();
        descriptors.add(CLIENT_ID);
        descriptors.add(CLIENT_SECRET);
        descriptors.add(USERNAME);
        descriptors.add(PASSWORD);
        descriptors.add(BASE_URL);
        descriptors.add(TOKEN_REQUEST_URL);

        this.descriptors = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<Relationship>();
        relationships.add(SUCCESS);
        relationships.add(FAILURE);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }

    @OnScheduled
    public void onScheduled(final ProcessContext context) {

    }

    @Override
    public void onTrigger(ProcessContext context, ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if ( flowFile == null ) {
            return;
        }

        String clientId = context.getProperty("CLIENT_ID")
                .evaluateAttributeExpressions().getValue();
        String clientSecret = context.getProperty("CLIENT_SECRET")
                .evaluateAttributeExpressions().getValue();
        String username = context.getProperty("USERNAME")
                .evaluateAttributeExpressions().getValue();
        String password = context.getProperty("PASSWORD")
                .evaluateAttributeExpressions().getValue();
        String baseURL = context.getProperty("BASE_URL")
                .evaluateAttributeExpressions().getValue();
        String tokenRequestURL = context.getProperty("TOKEN_REQUEST_URL")
                .evaluateAttributeExpressions().getValue();
        String objectName = flowFile.getAttribute("sfsc.object.name");
        SFSCClient client = new SFSCClient(clientId, clientSecret);
        OAuthUtil oAuthUtil = new OAuthUtil(client, username, password);
        try {
            DescribeSObjectResult result = ExecuteQueryRequest.getObjectMetadata(oAuthUtil,
                    baseURL, objectName);
            String output = new Gson().toJson(result.getFields());
            if(result != null && result.getFields() != null && result.getFields().size() > 0) {
                session.write(flowFile, new OutputStreamCallback() {
                    @Override
                    public void process(OutputStream outputStream) throws IOException {
                        outputStream.write(output.getBytes());
                        outputStream.flush();
                        outputStream.close();
                    }
                });
                session.getProvenanceReporter().modifyContent(flowFile);
                session.putAttribute(flowFile, "field.count", String.valueOf(result.getFields().size()));
                session.transfer(flowFile, SUCCESS);
            } else {
                session.putAttribute(flowFile, "field.count", "0");
                session.transfer(flowFile, FAILURE);
            }

        } catch (Exception e) {
            e.printStackTrace();
            getLogger().error("Exception while fetching data from SFSC: " + e.getMessage());
            session.transfer(flowFile, FAILURE);
        }
    }
}
